﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntelliGeoTranslator
{
    public partial class ReservedWords : Form
    {
        public ReservedWords()
        {
            InitializeComponent();
            richTextBox1.Text = File.ReadAllText("../../../Lexer/Resources/LexemeStorage.json");
        }
    }
}