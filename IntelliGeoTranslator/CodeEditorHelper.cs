﻿using IntelliGeoTranslator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliGeoTranslator
{
    public static class CodeEditorHelper
    {
        public static CaretPosition GetCaretPosition(string file, int selectionStart)
        {
            int row = 1;
            int column = 1;
            for (int i = 0; i < selectionStart; i++)
            {
                column++;
                if (file[i] == '\n')
                {
                    row++;
                    column = 1;
                }
            }

            return new CaretPosition { Column = column, Row = row };
        }
    }
}