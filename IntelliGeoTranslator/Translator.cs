﻿using IntelliGeoTranslator.Models;
using Lexer;
using Lexer.Models;
using Specs;
using Specs.Enums;
using Syntaxer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rule = Specs.Rule;

namespace IntelliGeoTranslator
{
    public partial class Translator : Form
    {
        private CaretPosition caretPosition = null;
        private bool fileChanged = false;
        private string filePath;

        public Translator()
        {
            InitializeComponent();
            splitContainer1.Panel2Collapsed = true;

            StringBuilder text = new StringBuilder();
            text.AppendLine("$Start");
            text.AppendLine("Int x");
            text.AppendLine("Read(x)");
            text.AppendLine("Int y = 1");
            text.AppendLine("Int test = 0 + 1");
            text.AppendLine("Do {");
            text.AppendLine("Print(y)");
            text.AppendLine("y = 2 + 1");
            text.AppendLine("test = x > y");
            text.AppendLine("} While(test)");
            text.AppendLine("Int MaxVal = Max(-12, -10)");
            text.AppendLine("Int MinVal = Min(12, 10)");
            text.AppendLine("Int PowVal = Pow(4, 3)");
            text.AppendLine("Double SqrtVal = Sqrt(99, 2)");
            text.AppendLine("Int FlooredValue = Floor(2.2)");
            text.AppendLine("$End");
            richTextBoxCodeEditor.Text = text.ToString();
        }

        public void AddLineNumbers()
        {
            Point pt = new Point(0, 0);
            int First_Index = richTextBoxCodeEditor.GetCharIndexFromPosition(pt);
            int First_Line = richTextBoxCodeEditor.GetLineFromCharIndex(First_Index);
            pt.X = ClientRectangle.Width;
            pt.Y = ClientRectangle.Height;
            int Last_Index = richTextBoxCodeEditor.GetCharIndexFromPosition(pt);
            int Last_Line = richTextBoxCodeEditor.GetLineFromCharIndex(Last_Index);
            richTextBoxCodeEditorLines.Text = "";
            string lines = "";

            for (int i = First_Line; i <= Last_Line + 1; i++)
            {
                lines += i + 1 + "\n";
            }

            richTextBoxCodeEditorLines.Text = lines;
            richTextBoxCodeEditorLines.SelectAll();
            richTextBoxCodeEditorLines.SelectionAlignment = HorizontalAlignment.Center;
        }

        private void richTextBoxCodeEditor_TextChanged(object sender, EventArgs e)
        {
            fileChanged = true;
        }

        private void richTextBoxCodeEditor_SelectionChanged(object sender, EventArgs e)
        {
            AddLineNumbers();

            caretPosition = CodeEditorHelper.GetCaretPosition(richTextBoxCodeEditor.Text, richTextBoxCodeEditor.SelectionStart);
            labelCursorPosition.Text = $"Line: {caretPosition.Row}, Column: {caretPosition.Column}";
        }

        private void richTextBoxCodeEditor_VScroll(object sender, EventArgs e)
        {
            AddLineNumbers();
        }

        private void Translator_Resize(object sender, EventArgs e)
        {
            AddLineNumbers();
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fileChanged)
            {
                var dialogResult = MessageBox.Show("Save changes?", "IntelliGeo Translator", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        if (!SaveFile())
                            return;
                        break;

                    case DialogResult.No:
                        break;

                    case DialogResult.Cancel:
                        return;
                }
            }

            richTextBoxCodeEditor.Text = "";
            fileChanged = false;
            filePath = null;
        }

        private bool SaveFile()
        {
            if (filePath == null)
            {
                if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return false;

                filePath = saveFileDialog1.FileName;
            }

            try
            {
                System.IO.File.WriteAllText(filePath, richTextBoxCodeEditor.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show($"File is not saved. Error: {e.Message}");
                return false;
            }

            ShowSavedLabel();

            return true;
        }

        private async Task ShowSavedLabel()
        {
            labelSaved.Visible = true;
            labelSaved.ForeColor = Color.FromArgb(64, 64, 64);

            for (int i = 64; i < 171; ++i)
            {
                labelSaved.ForeColor = Color.FromArgb(i, i, i);
                await Task.Delay(20);
            }
            labelSaved.Visible = false;
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            filePath = openFileDialog1.FileName;
            string fileText = System.IO.File.ReadAllText(filePath);
            richTextBoxCodeEditor.Text = fileText;
            AddLineNumbers();
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SaveFile())
            {
                fileChanged = false;
                ShowSavedLabel();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            try
            {
                filePath = saveFileDialog1.FileName;
                System.IO.File.WriteAllText(filePath, richTextBoxCodeEditor.Text);
                fileChanged = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show($"File is not saved. Error: {exc.Message}");
            }

            ShowSavedLabel();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fileChanged)
            {
                var dialogResult = MessageBox.Show("Save changes?", "IntelliGeo Translator", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        if (!SaveFile())
                            return;
                        break;

                    case DialogResult.No:
                        break;

                    case DialogResult.Cancel:
                        return;
                }
            }
            this.Close();
        }

        private void Translator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (fileChanged)
            {
                var dialogResult = MessageBox.Show("Save changes?", "IntelliGeo Translator", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        if (!SaveFile())
                            e.Cancel = true;
                        break;
                }
            }
        }

        private void analyzeCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = false;
            LexemeAnalyzer lexer = new LexemeAnalyzer();
            AnalyzeResult result = lexer.Analyze(richTextBoxCodeEditor.Text);
            //AnalyzeResult result = lexer.Analyze(text.ToString());
            FillLexemeList(result.Lexemes);
            FillIdentfiersList(result.Identifiers);
            FillErrorList(result.Errors);
            FillElementsTable(result);
            SyntaxAnalyzer SA = new SyntaxAnalyzer(lexer.lexemeStorage);
            SA.Build();
            SA.Run(result.Lexemes);
            FillPredictiveTableView(SA);
            FillOutputTextBox(SA.Output, lexer.lexemeStorage);

            SyntaxErrorsTextBox.Text = "";
            foreach (var i in SA.Errors)
            {
                SyntaxErrorsTextBox.Text += i.Text + '\n';
            }

        }

        private void FillOutputTextBox(List<Rule> rules, LexemeStorage lexemeStorage)
        {
            SAOutputTextBox.Text = "";
            int i = 0;
            foreach (Rule rule in rules)
            {
                for (int j = 0; j < rule.GrammarElements.Count; j++)
                {
                    if (rule.GrammarElements[j] as Lexeme != null && (rule.GrammarElements[j] as Lexeme).LexemeClass == LexemeClass.Literal)
                    {
                        SAOutputTextBox.Text += lexemeStorage.Literals[i];
                        i++;
                    }
                    else
                    {
                        if (rule.GrammarElements[j] is Nonterminal)
                        {
                            SAOutputTextBox.Text += " <"+ rule.GrammarElements[j].Value+"> ";
                        }
                        else SAOutputTextBox.Text +=  " "+rule.GrammarElements[j].Value + " ";
                    }
                }
                SAOutputTextBox.Text += '\n';
            }
            //SAOutputTextBox.Text += rule.ToString() + '\n';
        }

    private void FillPredictiveTableView(SyntaxAnalyzer SA)
    {

        PredictiveTableView.Rows.Clear();
        PredictiveTableView.Columns.Clear();
        PredictiveTableView.Refresh();
        foreach (var i in SA.Lexemes.Values)
        {
            PredictiveTableView.Columns.Add(i.Value + "Column", i.Value);
        }
        foreach (var i in SA.Nonterminals.Values)
        {
            PredictiveTableView.Rows.Add();
            PredictiveTableView.Rows[PredictiveTableView.Rows.Count - 1].HeaderCell.Value = i.Value;
        }
        int rows = SA.PredictiveTable.GetUpperBound(0) + 1;
        int columns = SA.PredictiveTable.Length / rows;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                PredictiveTableView.Rows[i].Cells[j].Value = SA.PredictiveTable[i, j]?.ToString() ?? "error";
            }
        }
    }

    private void FillLexemeList(List<Lexeme> lexemes)
    {
        dataGridViewLexemes.Rows.Clear();
        foreach (var lexeme in lexemes)
        {
            dataGridViewLexemes.Rows.Add(lexeme.Value, lexeme.LexemeClass, Math.Abs((int)lexeme.LexemeClass), lexeme.NumberInClass + 1, lexeme.LocationInCode.Get());
        }
    }

    private void FillIdentfiersList(List<Identifier> identifiers)
    {
        dataGridViewIdentifiers.Rows.Clear();
        foreach (var identifier in identifiers)
        {
            dataGridViewIdentifiers.Rows.Add(identifier.Id, identifier.Type, identifier.Name);
        }
    }

    private void FillErrorList(List<Error> errors)
    {
        dataGridViewErrors.Rows.Clear();
        foreach (var error in errors)
        {
            dataGridViewErrors.Rows.Add(error.Description, error.LocationInCode.Get());
        }
    }

    private void FillElementsTable(AnalyzeResult result)
    {
        richTextBoxElementsTable.ResetText();

        List<LocationInCode> locations = new List<LocationInCode>();
        locations.AddRange(result.Lexemes.Select(l => l.LocationInCode));
        locations.AddRange(result.Errors.Select(e => e.LocationInCode));
        locations.Sort();

        if (locations.Count > 0)
        {
            int currentLine = locations.First().Line;
            foreach (var location in locations)
            {
                string newLine = "";
                if (location.Line > currentLine)
                {
                    currentLine = location.Line;
                    newLine = "\n";
                }
                var lexeme = result.Lexemes.FirstOrDefault(l => l.LocationInCode == location);
                if (lexeme != null)
                    richTextBoxElementsTable.AppendText(newLine + $"{Math.Abs((int)lexeme.LexemeClass)}/{lexeme.NumberInClass + 1}  ");
                else
                {
                    var error = result.Errors.First(l => l.LocationInCode == location);
                    richTextBoxElementsTable.AppendText(newLine + $"{error.Description} in {error.LocationInCode.Get()}  ");
                }
            }
        }
    }

    private void reservedWordsToolStripMenuItem_Click(object sender, EventArgs e)
    {
        new ReservedWords().Show();
    }

    private void showAnalyzerOutputToolStripMenuItem_Click(object sender, EventArgs e)
    {
        splitContainer1.Panel2Collapsed = false;
    }

    private void hideAnalyzeWindowToolStripMenuItem_Click(object sender, EventArgs e)
    {
        splitContainer1.Panel2Collapsed = true;
    }
    }
}