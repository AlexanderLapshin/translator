﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliGeoTranslator.Models
{
    public class CaretPosition
    {
        public int Column { get; set; }
        public int Row { get; set; }
    }
}