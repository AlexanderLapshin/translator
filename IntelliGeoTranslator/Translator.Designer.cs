﻿namespace IntelliGeoTranslator
{
    partial class Translator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analyzeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analyzeCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAnalyzerOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideAnalyzeWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reservedWordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.labelSaved = new System.Windows.Forms.Label();
            this.labelCursorPosition = new System.Windows.Forms.Label();
            this.richTextBoxCodeEditor = new System.Windows.Forms.RichTextBox();
            this.richTextBoxCodeEditorLines = new System.Windows.Forms.RichTextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabLexemes = new System.Windows.Forms.TabPage();
            this.dataGridViewLexemes = new System.Windows.Forms.DataGridView();
            this.Lexeme = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClassName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClassNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberInClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabIdentifiers = new System.Windows.Forms.TabPage();
            this.dataGridViewIdentifiers = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.PredictiveTableView = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SAOutputTextBox = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.SyntaxErrorsTextBox = new System.Windows.Forms.RichTextBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBoxElementsTable = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewErrors = new System.Windows.Forms.DataGridView();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PositionInCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabLexemes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLexemes)).BeginInit();
            this.tabIdentifiers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIdentifiers)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PredictiveTableView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewErrors)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.analyzeToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1362, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileToolStripMenuItem,
            this.openFileToolStripMenuItem,
            this.saveFileToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newFileToolStripMenuItem
            // 
            this.newFileToolStripMenuItem.Name = "newFileToolStripMenuItem";
            this.newFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newFileToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.newFileToolStripMenuItem.Text = "New File";
            this.newFileToolStripMenuItem.Click += new System.EventHandler(this.newFileToolStripMenuItem_Click);
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.openFileToolStripMenuItem.Text = "Open File";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // saveFileToolStripMenuItem
            // 
            this.saveFileToolStripMenuItem.Name = "saveFileToolStripMenuItem";
            this.saveFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveFileToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.saveFileToolStripMenuItem.Text = "Save File";
            this.saveFileToolStripMenuItem.Click += new System.EventHandler(this.saveFileToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // analyzeToolStripMenuItem
            // 
            this.analyzeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analyzeCodeToolStripMenuItem,
            this.showAnalyzerOutputToolStripMenuItem,
            this.hideAnalyzeWindowToolStripMenuItem});
            this.analyzeToolStripMenuItem.Name = "analyzeToolStripMenuItem";
            this.analyzeToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.analyzeToolStripMenuItem.Text = "Analyze";
            // 
            // analyzeCodeToolStripMenuItem
            // 
            this.analyzeCodeToolStripMenuItem.Name = "analyzeCodeToolStripMenuItem";
            this.analyzeCodeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.A)));
            this.analyzeCodeToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.analyzeCodeToolStripMenuItem.Text = "Analyze Code";
            this.analyzeCodeToolStripMenuItem.Click += new System.EventHandler(this.analyzeCodeToolStripMenuItem_Click);
            // 
            // showAnalyzerOutputToolStripMenuItem
            // 
            this.showAnalyzerOutputToolStripMenuItem.Name = "showAnalyzerOutputToolStripMenuItem";
            this.showAnalyzerOutputToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.showAnalyzerOutputToolStripMenuItem.Text = "Show Analyzer Output";
            this.showAnalyzerOutputToolStripMenuItem.Click += new System.EventHandler(this.showAnalyzerOutputToolStripMenuItem_Click);
            // 
            // hideAnalyzeWindowToolStripMenuItem
            // 
            this.hideAnalyzeWindowToolStripMenuItem.Name = "hideAnalyzeWindowToolStripMenuItem";
            this.hideAnalyzeWindowToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.hideAnalyzeWindowToolStripMenuItem.Text = "Hide Analyzer Output";
            this.hideAnalyzeWindowToolStripMenuItem.Click += new System.EventHandler(this.hideAnalyzeWindowToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reservedWordsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // reservedWordsToolStripMenuItem
            // 
            this.reservedWordsToolStripMenuItem.Name = "reservedWordsToolStripMenuItem";
            this.reservedWordsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.reservedWordsToolStripMenuItem.Text = "Reserved Words";
            this.reservedWordsToolStripMenuItem.Click += new System.EventHandler(this.reservedWordsToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.labelSaved);
            this.splitContainer1.Panel1.Controls.Add(this.labelCursorPosition);
            this.splitContainer1.Panel1.Controls.Add(this.richTextBoxCodeEditor);
            this.splitContainer1.Panel1.Controls.Add(this.richTextBoxCodeEditorLines);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1362, 754);
            this.splitContainer1.SplitterDistance = 706;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 1;
            // 
            // labelSaved
            // 
            this.labelSaved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSaved.AutoSize = true;
            this.labelSaved.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSaved.Location = new System.Drawing.Point(1306, 738);
            this.labelSaved.Name = "labelSaved";
            this.labelSaved.Size = new System.Drawing.Size(43, 13);
            this.labelSaved.TabIndex = 3;
            this.labelSaved.Text = "Saved";
            this.labelSaved.Visible = false;
            // 
            // labelCursorPosition
            // 
            this.labelCursorPosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCursorPosition.AutoSize = true;
            this.labelCursorPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCursorPosition.Location = new System.Drawing.Point(7, 737);
            this.labelCursorPosition.Name = "labelCursorPosition";
            this.labelCursorPosition.Size = new System.Drawing.Size(35, 13);
            this.labelCursorPosition.TabIndex = 2;
            this.labelCursorPosition.Text = "Line:";
            // 
            // richTextBoxCodeEditor
            // 
            this.richTextBoxCodeEditor.AcceptsTab = true;
            this.richTextBoxCodeEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxCodeEditor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.richTextBoxCodeEditor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxCodeEditor.BulletIndent = 5;
            this.richTextBoxCodeEditor.Font = new System.Drawing.Font("Consolas", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBoxCodeEditor.ForeColor = System.Drawing.Color.White;
            this.richTextBoxCodeEditor.Location = new System.Drawing.Point(50, 0);
            this.richTextBoxCodeEditor.Name = "richTextBoxCodeEditor";
            this.richTextBoxCodeEditor.Size = new System.Drawing.Size(1311, 737);
            this.richTextBoxCodeEditor.TabIndex = 0;
            this.richTextBoxCodeEditor.Text = "";
            this.richTextBoxCodeEditor.WordWrap = false;
            this.richTextBoxCodeEditor.SelectionChanged += new System.EventHandler(this.richTextBoxCodeEditor_SelectionChanged);
            this.richTextBoxCodeEditor.VScroll += new System.EventHandler(this.richTextBoxCodeEditor_VScroll);
            this.richTextBoxCodeEditor.TextChanged += new System.EventHandler(this.richTextBoxCodeEditor_TextChanged);
            // 
            // richTextBoxCodeEditorLines
            // 
            this.richTextBoxCodeEditorLines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.richTextBoxCodeEditorLines.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.richTextBoxCodeEditorLines.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxCodeEditorLines.Font = new System.Drawing.Font("Consolas", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBoxCodeEditorLines.ForeColor = System.Drawing.Color.White;
            this.richTextBoxCodeEditorLines.Location = new System.Drawing.Point(1, 0);
            this.richTextBoxCodeEditorLines.Name = "richTextBoxCodeEditorLines";
            this.richTextBoxCodeEditorLines.ReadOnly = true;
            this.richTextBoxCodeEditorLines.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBoxCodeEditorLines.Size = new System.Drawing.Size(46, 737);
            this.richTextBoxCodeEditorLines.TabIndex = 1;
            this.richTextBoxCodeEditorLines.Text = "";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(654, 754);
            this.splitContainer2.SplitterDistance = 284;
            this.splitContainer2.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabLexemes);
            this.tabControl1.Controls.Add(this.tabIdentifiers);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(654, 284);
            this.tabControl1.TabIndex = 2;
            // 
            // tabLexemes
            // 
            this.tabLexemes.Controls.Add(this.dataGridViewLexemes);
            this.tabLexemes.Location = new System.Drawing.Point(4, 22);
            this.tabLexemes.Name = "tabLexemes";
            this.tabLexemes.Padding = new System.Windows.Forms.Padding(3);
            this.tabLexemes.Size = new System.Drawing.Size(646, 258);
            this.tabLexemes.TabIndex = 0;
            this.tabLexemes.Text = "Lexemes";
            this.tabLexemes.UseVisualStyleBackColor = true;
            // 
            // dataGridViewLexemes
            // 
            this.dataGridViewLexemes.AllowUserToAddRows = false;
            this.dataGridViewLexemes.AllowUserToDeleteRows = false;
            this.dataGridViewLexemes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.dataGridViewLexemes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLexemes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Lexeme,
            this.ClassName,
            this.ClassNumber,
            this.NumberInClass,
            this.Position});
            this.dataGridViewLexemes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewLexemes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.dataGridViewLexemes.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewLexemes.Name = "dataGridViewLexemes";
            this.dataGridViewLexemes.ReadOnly = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewLexemes.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewLexemes.Size = new System.Drawing.Size(640, 252);
            this.dataGridViewLexemes.TabIndex = 1;
            // 
            // Lexeme
            // 
            this.Lexeme.HeaderText = "Lexeme";
            this.Lexeme.Name = "Lexeme";
            this.Lexeme.ReadOnly = true;
            // 
            // ClassName
            // 
            this.ClassName.HeaderText = "Class Name";
            this.ClassName.Name = "ClassName";
            this.ClassName.ReadOnly = true;
            // 
            // ClassNumber
            // 
            this.ClassNumber.HeaderText = "Class Number";
            this.ClassNumber.Name = "ClassNumber";
            this.ClassNumber.ReadOnly = true;
            // 
            // NumberInClass
            // 
            this.NumberInClass.HeaderText = "Number in Class";
            this.NumberInClass.Name = "NumberInClass";
            this.NumberInClass.ReadOnly = true;
            // 
            // Position
            // 
            this.Position.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Position.HeaderText = "Position in Code";
            this.Position.Name = "Position";
            this.Position.ReadOnly = true;
            // 
            // tabIdentifiers
            // 
            this.tabIdentifiers.Controls.Add(this.dataGridViewIdentifiers);
            this.tabIdentifiers.Location = new System.Drawing.Point(4, 22);
            this.tabIdentifiers.Name = "tabIdentifiers";
            this.tabIdentifiers.Padding = new System.Windows.Forms.Padding(3);
            this.tabIdentifiers.Size = new System.Drawing.Size(646, 258);
            this.tabIdentifiers.TabIndex = 1;
            this.tabIdentifiers.Text = "Identifiers";
            this.tabIdentifiers.UseVisualStyleBackColor = true;
            // 
            // dataGridViewIdentifiers
            // 
            this.dataGridViewIdentifiers.AllowUserToAddRows = false;
            this.dataGridViewIdentifiers.AllowUserToDeleteRows = false;
            this.dataGridViewIdentifiers.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.dataGridViewIdentifiers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIdentifiers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dataGridViewIdentifiers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewIdentifiers.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.dataGridViewIdentifiers.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewIdentifiers.Name = "dataGridViewIdentifiers";
            this.dataGridViewIdentifiers.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewIdentifiers.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewIdentifiers.Size = new System.Drawing.Size(640, 252);
            this.dataGridViewIdentifiers.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Type";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Name";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.PredictiveTableView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(646, 258);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "ParsingTable";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // PredictiveTableView
            // 
            this.PredictiveTableView.AllowUserToAddRows = false;
            this.PredictiveTableView.AllowUserToDeleteRows = false;
            this.PredictiveTableView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PredictiveTableView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PredictiveTableView.Location = new System.Drawing.Point(3, 3);
            this.PredictiveTableView.Name = "PredictiveTableView";
            this.PredictiveTableView.ReadOnly = true;
            this.PredictiveTableView.Size = new System.Drawing.Size(640, 252);
            this.PredictiveTableView.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.SAOutputTextBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(646, 258);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Parsing Output";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SAOutputTextBox
            // 
            this.SAOutputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SAOutputTextBox.Location = new System.Drawing.Point(3, 3);
            this.SAOutputTextBox.Name = "SAOutputTextBox";
            this.SAOutputTextBox.Size = new System.Drawing.Size(640, 252);
            this.SAOutputTextBox.TabIndex = 0;
            this.SAOutputTextBox.Text = "";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.SyntaxErrorsTextBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(646, 258);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Syntax Errors";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // SyntaxErrorsTextBox
            // 
            this.SyntaxErrorsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SyntaxErrorsTextBox.Location = new System.Drawing.Point(3, 3);
            this.SyntaxErrorsTextBox.Name = "SyntaxErrorsTextBox";
            this.SyntaxErrorsTextBox.Size = new System.Drawing.Size(640, 252);
            this.SyntaxErrorsTextBox.TabIndex = 1;
            this.SyntaxErrorsTextBox.Text = "";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer3.Size = new System.Drawing.Size(654, 466);
            this.splitContainer3.SplitterDistance = 333;
            this.splitContainer3.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.groupBox1.Controls.Add(this.richTextBoxElementsTable);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(654, 333);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Elements  Table";
            // 
            // richTextBoxElementsTable
            // 
            this.richTextBoxElementsTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.richTextBoxElementsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxElementsTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBoxElementsTable.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBoxElementsTable.Location = new System.Drawing.Point(3, 18);
            this.richTextBoxElementsTable.Name = "richTextBoxElementsTable";
            this.richTextBoxElementsTable.ReadOnly = true;
            this.richTextBoxElementsTable.Size = new System.Drawing.Size(648, 312);
            this.richTextBoxElementsTable.TabIndex = 0;
            this.richTextBoxElementsTable.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.groupBox2.Controls.Add(this.dataGridViewErrors);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(654, 129);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lexeme Errors";
            // 
            // dataGridViewErrors
            // 
            this.dataGridViewErrors.AllowUserToAddRows = false;
            this.dataGridViewErrors.AllowUserToDeleteRows = false;
            this.dataGridViewErrors.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.dataGridViewErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewErrors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Description,
            this.PositionInCode});
            this.dataGridViewErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewErrors.Location = new System.Drawing.Point(3, 18);
            this.dataGridViewErrors.Name = "dataGridViewErrors";
            this.dataGridViewErrors.ReadOnly = true;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewErrors.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewErrors.Size = new System.Drawing.Size(648, 108);
            this.dataGridViewErrors.TabIndex = 0;
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            // 
            // PositionInCode
            // 
            this.PositionInCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PositionInCode.HeaderText = "Position in Code";
            this.PositionInCode.Name = "PositionInCode";
            this.PositionInCode.ReadOnly = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Translator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(177)))), ((int)(((byte)(186)))));
            this.ClientSize = new System.Drawing.Size(1362, 778);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Translator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IntelliGeo Translator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Translator_FormClosing);
            this.Resize += new System.EventHandler(this.Translator_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabLexemes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLexemes)).EndInit();
            this.tabIdentifiers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIdentifiers)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PredictiveTableView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewErrors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richTextBoxCodeEditor;
        private System.Windows.Forms.RichTextBox richTextBoxCodeEditorLines;
        private System.Windows.Forms.Label labelCursorPosition;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem newFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label labelSaved;
        private System.Windows.Forms.ToolStripMenuItem analyzeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reservedWordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analyzeCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAnalyzerOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hideAnalyzeWindowToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBoxElementsTable;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewErrors;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn PositionInCode;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabLexemes;
        private System.Windows.Forms.DataGridView dataGridViewLexemes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lexeme;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClassName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClassNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberInClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn Position;
        private System.Windows.Forms.TabPage tabIdentifiers;
        private System.Windows.Forms.DataGridView dataGridViewIdentifiers;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView PredictiveTableView;
        private System.Windows.Forms.RichTextBox SAOutputTextBox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox SyntaxErrorsTextBox;
    }
}

