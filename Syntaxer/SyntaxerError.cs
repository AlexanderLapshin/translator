﻿using Specs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Syntaxer
{
    public class SyntaxerError
    {
        public string Text { get; }
        public LocationInCode LocationInCode { get; }
        public SyntaxerError(string text, LocationInCode location)
        {
            LocationInCode = location;
            Text = text;
        }
    }
}
