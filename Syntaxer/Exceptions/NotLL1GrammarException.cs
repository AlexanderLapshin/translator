﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Syntaxer.Exceptions
{
    public class NotLL1GrammarException : Exception
    {
        public NotLL1GrammarException()
        {
        }

        public NotLL1GrammarException(string message) : base(message)
        {
        }

        public NotLL1GrammarException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotLL1GrammarException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
