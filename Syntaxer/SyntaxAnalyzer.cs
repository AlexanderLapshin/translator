﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lexer;
using Lexer.Models;
using Specs;
using Specs.Enums;
using Syntaxer.Exceptions;

namespace Syntaxer
{
    public class SyntaxAnalyzer
    {
        public Nonterminal S { get; }
        public IDictionary<string, Nonterminal> Nonterminals { get; }
        public IDictionary<string, Lexeme> Lexemes { get; }
        public Rule[,] PredictiveTable { get; private set; }
        public Stack<GrammarElement> Stack { get; }
        public bool AnalyzeCompleted { get; private set; }
        public LexemeStorage LexemeStorage { get; }
        public List<SyntaxerError> Errors { get; }
        public List<Rule> Output { get; }
        public SyntaxAnalyzer(LexemeStorage storage)
        {
            LexemeStorage = storage;
            S = new Nonterminal("S");
            Errors = new List<SyntaxerError>();
            AnalyzeCompleted = false;
            Stack = new Stack<GrammarElement>();
            Output = new List<Rule>();
            //Lexemes = new Dictionary<string, Lexeme>
            //{
            //    { "$", Helper.Stop },
            //    { "+", new Lexeme(LexemeClass.Operator, 500, null, "+") },
            //    { "*", new Lexeme(LexemeClass.Operator, 600, null, "*") },
            //    { "<", new Lexeme(LexemeClass.Operator, 700, null, "<") },
            //    { ">", new Lexeme(LexemeClass.Operator, 800, null, ">") },
            //    { "Int", new Lexeme(LexemeClass.Type, 1, null, "Int") },
            //    { Helper.Lambda.Value, Helper.Lambda },
            //};
            //Nonterminals = new Dictionary<string, Nonterminal>
            //{
            //    { S.Value, S },
            //    { "E", new Nonterminal("E") },
            //    { "T", new Nonterminal("T") },
            //    { "E'", new Nonterminal("E'") },
            //    { "T'", new Nonterminal("T'") },
            //    { "F", new Nonterminal("F") },
            //};
            //Nonterminals["E"].Rules.Add(new Rule(Nonterminals["T"], Nonterminals["E'"]));
            //Nonterminals["E'"].Rules.Add(new Rule(Lexemes["+"], Nonterminals["T"], Nonterminals["E'"]));
            //Nonterminals["E'"].Rules.Add(new Rule(Helper.Lambda));
            //Nonterminals["T"].Rules.Add(new Rule(Nonterminals["F"], Nonterminals["T'"]));
            //Nonterminals["T'"].Rules.Add(new Rule(Lexemes["*"], Nonterminals["F"], Nonterminals["T'"]));
            //Nonterminals["T'"].Rules.Add(new Rule(Helper.Lambda));
            //Nonterminals["F"].Rules.Add(new Rule(Lexemes["<"], Nonterminals["E"], Lexemes[">"]));
            //Nonterminals["F"].Rules.Add(new Rule(Lexemes["Int"]));


            storage.Identifiers.Add(new Identifier
            {
                Id = storage.Identifiers.Count + 1,
                Name = "Bool identifier ",
                Type = 2
            });
            storage.Identifiers.Add(new Identifier
            {
                Id = storage.Identifiers.Count + 1,
                Name = "Integer identifier ",
                Type = 0
            });
            storage.Identifiers.Add(new Identifier
            {
                Id = storage.Identifiers.Count + 1,
                Name = "Identifier",
                Type = -1
            });

            Lexemes = new Dictionary<string, Lexeme>
            {
                { "Int", new Lexeme("Int", LexemeClass.Type) },
                { "Double", new Lexeme("Double", LexemeClass.Type) },
                { "Bool", new Lexeme("Bool", LexemeClass.Type) },
                { "Identifier", new Lexeme("Identifier", LexemeClass.Identifier, storage.Identifiers.Count - 1) },
                { "Integer identifier", new Lexeme("Integer identifier", LexemeClass.Identifier, storage.Identifiers.Count - 2) },
                { "Bool identifier", new Lexeme("Bool identifier", LexemeClass.Identifier, storage.Identifiers.Count - 3) },
                { "-", new Lexeme("-", LexemeClass.Operator) },
                { "+", new Lexeme("+", LexemeClass.Operator) },
                { "*", new Lexeme("*", LexemeClass.Operator) },
                { "/", new Lexeme("/", LexemeClass.Operator) },
                { "=", new Lexeme("=", LexemeClass.Operator) },
                { "!=", new Lexeme("!=", LexemeClass.Operator) },
                { "%", new Lexeme("%", LexemeClass.Operator) },
                { "&&", new Lexeme("&&", LexemeClass.Operator) },
                { "<", new Lexeme("<", LexemeClass.Operator) },
                { ">", new Lexeme(">", LexemeClass.Operator) },
                { "<=", new Lexeme("<=", LexemeClass.Operator) },
                { ">=", new Lexeme(">=", LexemeClass.Operator) },
                { "||", new Lexeme("||", LexemeClass.Operator) },
                { "==", new Lexeme("==", LexemeClass.Operator) },
                { "{", new Lexeme("{", LexemeClass.Separator) },
                { "}", new Lexeme("}", LexemeClass.Separator) },
                { "(", new Lexeme("(", LexemeClass.Separator) },
                { ")", new Lexeme(")", LexemeClass.Separator) },
                { ";", new Lexeme(";", LexemeClass.Separator) },
                { ",", new Lexeme(",", LexemeClass.Separator) },
                { "$Start", new Lexeme("$Start", LexemeClass.ReservedWord) },
                { "$End", new Lexeme("$End", LexemeClass.ReservedWord) },
                { "Read", new Lexeme("Read", LexemeClass.Function) },
                { "$", Helper.Stop },
                { "Integer literal", new Lexeme("13", LexemeClass.Literal) },
                { "Double literal", new Lexeme("1.1", LexemeClass.Literal) },
                { "True", new Lexeme("True", LexemeClass.ReservedWord) },
                { "False", new Lexeme("False", LexemeClass.ReservedWord) },
                { "Do", new Lexeme("Do", LexemeClass.ReservedWord) },
                { "While", new Lexeme("While", LexemeClass.ReservedWord) },
                { "Print", new Lexeme("Print", LexemeClass.Function) },
                { "Max", new Lexeme("Max", LexemeClass.Function) },
                { "Min", new Lexeme("Min", LexemeClass.Function) },
                { "Round", new Lexeme("Round", LexemeClass.Function) },
                { "Pow", new Lexeme("Pow", LexemeClass.Function) },
                { "Sqrt", new Lexeme("Sqrt", LexemeClass.Function) },
                { "Floor", new Lexeme("Floor", LexemeClass.Function) },
            };

            Nonterminals = new Dictionary<string, Nonterminal>
            {
                { "S", new Nonterminal("S") }, //аксиома грамматики
                { "Type", new Nonterminal("Type") }, //тип
                { "Variable declaration", new Nonterminal("Variable declaration") }, //объявление переменной
                { "Math operator", new Nonterminal("Math operator") }, //Математический оператор
                { "Boolean operator", new Nonterminal("Boolean operator") }, //Булевый оператор
                { "Math operation", new Nonterminal("Math operation") }, //математическая операция
                { "Code", new Nonterminal("Code") }, //Код
                { "Literal", new Nonterminal("Literal") }, //литерал
                { "Number", new Nonterminal("Number") }, //число 
                { "Assignment", new Nonterminal("Assignment") },
                { "Bool literal", new Nonterminal("Bool literal") },
                { "Formula", new Nonterminal("Formula") },
                { "Variable initialization", new Nonterminal("Variable initialization") },
                { "Assign identifier", new Nonterminal("Assign identifier") },
                { "Assign literal", new Nonterminal("Assign literal") },
                { "DoWhile cycle", new Nonterminal("DoWhile cycle") },
                { "Body cycle", new Nonterminal("Body cycle") },
                { "Assignable", new Nonterminal("Assignable") },
                { "Print", new Nonterminal("Print") },
                { "Max", new Nonterminal("Max") },
                { "Min", new Nonterminal("Min") },
                { "Round", new Nonterminal("Round") },
                { "Pow", new Nonterminal("Pow") },
                { "Sqrt", new Nonterminal("Sqrt") },
                { "Floor", new Nonterminal("Floor") },
                { "Sign", new Nonterminal("Sign") },
                { "Boolean statement", new Nonterminal("Boolean statement") },
                { "Formula element", new Nonterminal("Formula element") },
                { "Complex boolean statement", new Nonterminal("Complex boolean statement") },
                { "Boolean comprasion", new Nonterminal("Boolean comprasion") },
                { "Math operation element", new Nonterminal("Math operation element") },
                { "Input operator", new Nonterminal("Input operator") },
                { "Output operator", new Nonterminal("Output operator") },
                { "Function call", new Nonterminal("Function call") },
                { "Math expression", new Nonterminal("Math expression") },
                { "Operator", new Nonterminal("Operator") },
            };

            // Литерал типа boolean
            Nonterminals["Bool literal"].Rules.Add(new Rule(Lexemes["True"]));
            Nonterminals["Bool literal"].Rules.Add(new Rule(Lexemes["False"]));

            // Вызовы функций
            Nonterminals["Pow"].Rules.Add(new Rule(Lexemes["Pow"], Lexemes["("], Nonterminals["Assignable"], Lexemes[","], Nonterminals["Assignable"], Lexemes[")"]));
            Nonterminals["Max"].Rules.Add(new Rule(Lexemes["Max"], Lexemes["("], Nonterminals["Assignable"], Lexemes[","], Nonterminals["Assignable"], Lexemes[")"]));
            Nonterminals["Min"].Rules.Add(new Rule(Lexemes["Min"], Lexemes["("], Nonterminals["Assignable"], Lexemes[","], Nonterminals["Assignable"], Lexemes[")"]));
            Nonterminals["Round"].Rules.Add(new Rule(Lexemes["Round"], Lexemes["("], Nonterminals["Assignable"], Lexemes[","], Nonterminals["Assignable"], Lexemes[")"]));
            Nonterminals["Floor"].Rules.Add(new Rule(Lexemes["Floor"], Lexemes["("], Lexemes["Double literal"], Lexemes[")"]));
            Nonterminals["Sqrt"].Rules.Add(new Rule(Lexemes["Sqrt"], Lexemes["("], Nonterminals["Assignable"], Lexemes[","], Nonterminals["Assignable"], Lexemes[")"]));

            // Оператор ввода
            Nonterminals["Input operator"].Rules.Add(new Rule(Lexemes["Read"], Lexemes["("], Lexemes["Identifier"], Lexemes[")"]));

            // Оператор вывода
            Nonterminals["Output operator"].Rules.Add(new Rule(Lexemes["Print"], Lexemes["("], Lexemes["Identifier"], Lexemes[")"]));

            // Тип
            Nonterminals["Type"].Rules.Add(new Rule(Lexemes["Int"]));
            Nonterminals["Type"].Rules.Add(new Rule(Lexemes["Double"]));
            Nonterminals["Type"].Rules.Add(new Rule(Lexemes["Bool"]));

            // Math operator
            Nonterminals["Math operator"].Rules.Add(new Rule(Lexemes["-"]));
            Nonterminals["Math operator"].Rules.Add(new Rule(Lexemes["+"]));
            Nonterminals["Math operator"].Rules.Add(new Rule(Lexemes["*"]));
            Nonterminals["Math operator"].Rules.Add(new Rule(Lexemes["/"]));

            //Boolean operator
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes["&&"]));
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes["||"]));
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes[">"]));
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes["<"]));
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes["=="]));
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes[">="]));
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes["<="]));
            Nonterminals["Boolean operator"].Rules.Add(new Rule(Lexemes["!="]));


            // Variable declaration
            Nonterminals["Variable declaration"].Rules.Add(new Rule(Nonterminals["Type"], Lexemes["Identifier"], Nonterminals["Assignment"]));
            // Variable initialization
            Nonterminals["Variable initialization"].Rules.Add(new Rule(Lexemes["Identifier"], Nonterminals["Assignment"]));

            // Code
            Nonterminals["Code"].Rules.Add(new Rule(Nonterminals["Variable declaration"], Nonterminals["Code"]));
            Nonterminals["Code"].Rules.Add(new Rule(Nonterminals["DoWhile cycle"], Nonterminals["Code"]));
            Nonterminals["Code"].Rules.Add(new Rule(Nonterminals["Input operator"], Nonterminals["Code"]));
            Nonterminals["Code"].Rules.Add(new Rule(Nonterminals["Output operator"], Nonterminals["Code"]));
            Nonterminals["Code"].Rules.Add(new Rule(Nonterminals["Variable initialization"], Nonterminals["Code"]));
            Nonterminals["Code"].Rules.Add(new Rule(Nonterminals["Function call"], Nonterminals["Code"]));
            Nonterminals["Code"].Rules.Add(new Rule(Helper.Lambda));

            // S
            //Nonterminals["S"].Rules.Add(new Rule(Nonterminals["E"]));
            Nonterminals["S"].Rules.Add(new Rule(Lexemes["$Start"], Nonterminals["Code"], Lexemes["$End"]));

            //Function call
            Nonterminals["Function call"].Rules.Add(new Rule(Nonterminals["Pow"]));
            Nonterminals["Function call"].Rules.Add(new Rule(Nonterminals["Max"]));
            Nonterminals["Function call"].Rules.Add(new Rule(Nonterminals["Min"]));
            Nonterminals["Function call"].Rules.Add(new Rule(Nonterminals["Round"]));
            Nonterminals["Function call"].Rules.Add(new Rule(Nonterminals["Sqrt"]));
            Nonterminals["Function call"].Rules.Add(new Rule(Nonterminals["Floor"]));

            // Assignable
            //Nonterminals["Assignable"].Rules.Add(new Rule(Nonterminals["Literal"]));
            //Nonterminals["Assignable"].Rules.Add(new Rule(Lexemes["Identifier"]));
            Nonterminals["Assignable"].Rules.Add(new Rule(Nonterminals["Function call"]));
            //Nonterminals["Assignable"].Rules.Add(new Rule(Nonterminals["Boolean statement"]));
            Nonterminals["Assignable"].Rules.Add(new Rule(Nonterminals["Math operation"]));

            // Operator
            Nonterminals["Operator"].Rules.Add(new Rule(Nonterminals["Math operator"]));
            Nonterminals["Operator"].Rules.Add(new Rule(Nonterminals["Boolean operator"]));


            //Formula element
            Nonterminals["Formula element"].Rules.Add(new Rule(Nonterminals["Literal"]));
            Nonterminals["Formula element"].Rules.Add(new Rule(Lexemes["Identifier"]));

            // Math expression
            Nonterminals["Math expression"].Rules.Add(new Rule(Nonterminals["Operator"], Nonterminals["Formula element"], Nonterminals["Math expression"]));
            Nonterminals["Math expression"].Rules.Add(new Rule(Helper.Lambda));

            //Boolean statement
            //Nonterminals["Boolean statement"].Rules.Add(new Rule(Nonterminals["Formula element"], Nonterminals["Boolean operator"], Nonterminals["Formula element"]));

            // Math operation 
            Nonterminals["Math operation"].Rules.Add(new Rule(Nonterminals["Formula element"], Nonterminals["Math expression"]));


            // Assignment
            Nonterminals["Assignment"].Rules.Add(new Rule(Lexemes["="], Nonterminals["Assignable"]));
            Nonterminals["Assignment"].Rules.Add(new Rule(Helper.Lambda));

            // Sign
            Nonterminals["Sign"].Rules.Add(new Rule(Lexemes["+"]));
            Nonterminals["Sign"].Rules.Add(new Rule(Lexemes["-"]));


            // Number
            Nonterminals["Number"].Rules.Add(new Rule(Lexemes["Double literal"]));
            Nonterminals["Number"].Rules.Add(new Rule(Lexemes["Integer literal"]));
            Nonterminals["Number"].Rules.Add(new Rule(Nonterminals["Sign"], Nonterminals["Number"]));

            // Literal
            Nonterminals["Literal"].Rules.Add(new Rule(Nonterminals["Number"]));
            Nonterminals["Literal"].Rules.Add(new Rule(Nonterminals["Bool literal"]));

            // DoWhile cycle
            Nonterminals["DoWhile cycle"].Rules.Add(new Rule(Lexemes["Do"], Lexemes["{"], Nonterminals["Code"], Lexemes["}"], Lexemes["While"], Lexemes["("], Nonterminals["Math operation"], Lexemes[")"]));

            PredictiveTable = new Rule[Nonterminals.Values.Count, Lexemes.Values.Count];
        }

        private int GetLexemeIndex(Lexeme lexeme)
        {
            for (int j = 0; j < Lexemes.Values.Count; j++)
            {
                if (CompareLexemes(Lexemes.Values.ElementAt(j), lexeme))
                {
                    return j;
                }
            }
            return -1;
        }

        private void SetRule(Nonterminal nonterminal, Lexeme lexeme, Rule rule)
        {
            int i = Nonterminals.Values.ToList().IndexOf(nonterminal);
            int j = GetLexemeIndex(lexeme);
            PredictiveTable[i, j] = rule;
        }

        private Rule GetRule(Nonterminal nonterminal, Lexeme lexeme)
        {
            int i = Nonterminals.Values.ToList().IndexOf(nonterminal);
            int j = GetLexemeIndex(lexeme);
            if (j < 0)
            {
                return null;
            }
            return PredictiveTable[i, j];
        }

        private void BuildFollow()
        {
            S.Follow.Add(Helper.Stop);
            //Nonterminals["E"].Follow.Add(Helper.Stop);
            bool changed = true;
            while (changed)
            {
                changed = false;
                int count = Nonterminals.Values.Sum(m => m.Follow.Count);

                foreach (Nonterminal nonterminal in Nonterminals.Values)
                {
                    foreach (Rule rule in nonterminal.Rules)
                    {

                        for (int i = 0; i < rule.GrammarElements.Count - 1; i++)
                        {
                            if (rule.GrammarElements[i] as Nonterminal != null)
                            {
                                Nonterminal current = (Nonterminal)rule.GrammarElements[i];
                                GrammarElement next = rule.GrammarElements[i + 1];
                                current.AddRangeToHashSet(current.Follow, next.First.Where(m => m != Helper.Lambda).ToList());
                                if (next.First.Contains(Helper.Lambda))
                                {
                                    current.AddRangeToHashSet(current.Follow, nonterminal.Follow);
                                }
                            }
                        }

                        Nonterminal last = rule.GrammarElements.Last() as Nonterminal;
                        if (last != null)
                        {
                            last.AddRangeToHashSet(last.Follow, nonterminal.Follow);
                        }
                    }
                }
                if (count != Nonterminals.Values.Sum(m => m.Follow.Count))
                {
                    changed = true;
                }
            }
        }

        private void BuildFirst()
        {
            foreach (Lexeme lexeme in Lexemes.Values)
            {
                lexeme.BuildFirst();
            }
            foreach (Nonterminal nonterminal in Nonterminals.Values)
            {
                nonterminal.BuildFirst();
            }
        }

        private void CheckGrammar()
        {
            List<Lexeme> first = new List<Lexeme>();
            foreach (Nonterminal nonterminal in Nonterminals.Values)
            {
                bool lambda = false;
                foreach (Rule rule in nonterminal.Rules)
                {
                    foreach (Lexeme lexeme in rule.GrammarElements.First().First)
                    {
                        if (first.Contains(lexeme) || (lambda && lexeme == Helper.Lambda))
                        {
                            throw new NotLL1GrammarException();
                        }
                        if (lexeme == Helper.Lambda)
                        {
                            //Если β=> λ, то а не порождает ни одну строку, начинающуюся с терминала из FOLLOW(A).
                            lambda = true;
                        }
                    }
                    first.AddRange(rule.GrammarElements.First().First);
                }
                first.Clear();
                if (lambda)
                {
                    List<Lexeme> follow = new List<Lexeme>();
                    foreach (Rule rule in nonterminal.Rules)
                    {
                        foreach (Lexeme lexeme in rule.GrammarElements.First().First)
                        {

                        }
                    }
                }
            }
            //throw new NotLL1GrammarException();
        }

        private void BuildPredictiveTable()
        {
            foreach (Nonterminal nonterminal in Nonterminals.Values)
            {
                foreach (Rule rule in nonterminal.Rules)
                {
                    foreach (Lexeme lexeme in rule.GrammarElements.First().First)
                    {
                        if (lexeme != Helper.Lambda)
                        {
                            SetRule(nonterminal, lexeme, rule);
                        }
                    }
                    if (nonterminal.First.Contains(Helper.Lambda))
                    {
                        foreach (Lexeme lexeme in nonterminal.Follow)
                        {
                            if (lexeme != Helper.Lambda)
                            {
                                SetRule(nonterminal, lexeme, rule);
                            }
                        }
                    }
                    if (nonterminal.First.Contains(Helper.Lambda) && nonterminal.Follow.Contains(Helper.Stop))
                    {
                        SetRule(nonterminal, Helper.Stop, rule);
                    }
                }
            }
        }

        private void Clear()
        {
            AnalyzeCompleted = false;
            Stack.Clear();
            Errors.Clear();
            Output.Clear();
            Stack.Push(Helper.Stop);
            Stack.Push(Nonterminals["S"]);
        }

        private bool CompareLexemes(Lexeme lexeme1, Lexeme lexeme2)
        {
            if (lexeme1.LexemeClass == lexeme2.LexemeClass)
            {
                if (lexeme1.LexemeClass == LexemeClass.Identifier)
                {
                    return true;
                }
                if (lexeme1.LexemeClass == LexemeClass.Literal)
                {
                    bool bothAreInt = int.TryParse(lexeme1.Value, out int i) && int.TryParse(lexeme2.Value, out i);
                    bool bothAreDouble = double.TryParse(lexeme1.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out double d) && double.TryParse(lexeme2.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out d);
                    bool bothAreBool = bool.TryParse(lexeme1.Value, out bool b) && bool.TryParse(lexeme2.Value, out b);
                    return bothAreDouble || bothAreInt || bothAreBool;
                }
                return lexeme1.Equals(lexeme2);
            }
            return false;
        }

        public void Build()
        {
            foreach (Nonterminal nonterminal in Nonterminals.Values)
            {
                nonterminal.First.Clear();
                nonterminal.Follow.Clear();
            }

            BuildFirst();
            BuildFollow();
            CheckGrammar();
            BuildPredictiveTable();
        }

        public void ContextIsOk()
        {
        }

        public void Run(List<Lexeme> lexemes)
        {
            Clear();
            lexemes.Add(Helper.Stop);
            IEnumerator<Lexeme> lexeme = lexemes.GetEnumerator();
            lexeme.MoveNext();
            GrammarElement X;

            do
            {
                X = Stack.Peek();
                if (X is Lexeme)
                {
                    if (CompareLexemes((Lexeme)X, Helper.Stop))
                    {
                        if (!CompareLexemes(lexeme.Current, Helper.Stop))
                        {
                            Errors.Add(new SyntaxerError("Syntax error", lexeme.Current.LocationInCode));
                        }
                        AnalyzeCompleted = true;
                        return;
                    }
                    if (CompareLexemes((Lexeme)X, lexeme.Current))
                    {
                        Stack.Pop();
                        lexeme.MoveNext();
                    }
                    else
                    {
                        Errors.Add(new SyntaxerError("Input chain does not belong to the language", lexeme.Current.LocationInCode));
                        break;
                    }
                }
                else if (X is Nonterminal)
                {
                    Rule rule = GetRule((Nonterminal)X, lexeme.Current);
                    if (rule != null /*&& CheckContext((Nonterminal)X, lexeme.Current)*/)
                    {
                        Stack.Pop();
                        GrammarElement first = rule.GrammarElements.FirstOrDefault();
                        if (first.Value != Helper.Lambda.Value)
                        {
                            for (int i = rule.GrammarElements.Count - 1; i >= 0; i--)
                            {
                                Stack.Push(rule.GrammarElements[i]);
                            }
                        }
                        Output.Add(rule);
                    }
                    else
                    {
                        Errors.Add(new SyntaxerError("Predictive table cell is error!", lexeme.Current.LocationInCode));
                        break;
                    }
                }
            } while (!X.Equals(Helper.Stop));
            if (!lexeme.Current.Equals(Helper.Stop))
            {
                Errors.Add(new SyntaxerError("String was not completely analyzed!", lexeme.Current.LocationInCode));
            }
            lexemes.RemoveAt(lexemes.Count - 1);
        }

        public string GetOutput()
        {
            return "";
        }
    }
}
