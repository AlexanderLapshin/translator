﻿using Specs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lexer.Models
{
    public class AnalyzeResult
    {
        public List<Lexeme> Lexemes { get; private set; }
        public List<Error> Errors { get; private set; }
        public List<Identifier> Identifiers { get; private set; }

        public AnalyzeResult(List<Lexeme> lexemes, List<Identifier> identifiers, List<Error> errors)
        {
            Lexemes = lexemes;
            Identifiers = identifiers;
            Errors = errors;
        }
    }
}