﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lexer.Models
{
    public class State
    {
        public int StateNumber { get; set; }
        public List<NextState> NextStates { get; set; }
    }
}