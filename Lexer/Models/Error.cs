﻿using Specs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lexer.Models
{
    public class Error
    {
        public string Description { get; private set; }
        public LocationInCode LocationInCode { get; private set; }

        public Error(string description, LocationInCode locationInCode)
        {
            Description = description;
            LocationInCode = locationInCode;
        }

        public string GetDescription()
        {
            return $"Decription: {Description}. {LocationInCode.Get()}";
        }
    }
}