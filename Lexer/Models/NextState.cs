﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lexer.Models
{
    public class NextState
    {
        public string Value { get; set; }
        public int StateNumber { get; set; }
    }
}