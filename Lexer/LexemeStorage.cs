﻿using Lexer.Models;
using Specs;
using Specs.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lexer
{
    public class LexemeStorage
    {
        public List<string> ReservedWords { get; private set; } = new List<string>();
        public List<string> Types { get; private set; } = new List<string>();
        public List<string> Operators { get; private set; } = new List<string>();
        public List<string> Separators { get; private set; } = new List<string>();
        public List<string> Functions { get; private set; } = new List<string>();
        public List<Identifier> Identifiers { get; private set; } = new List<Identifier>();
        public List<string> Literals { get; private set; } = new List<string>();
        public List<Lexeme> Lexemes { get; private set; } = new List<Lexeme>();

        public void AddLexeme(string lexeme, LexemeClass lexemeClass, LocationInCode locationInCode)
        {
            switch (lexemeClass)
            {
                case LexemeClass.Identifier:
                    if (Functions.Contains(lexeme))
                    {
                        lexemeClass = LexemeClass.Function;
                        break;
                    }
                    if(Identifiers.Select(i => i.Name).Contains(lexeme) && Lexemes.Last().LexemeClass == LexemeClass.Type)
                    {                        
                       throw new Exception($"The variable '{lexeme}' is already defined");    
                    }
                    else if (!Identifiers.Select(i => i.Name).Contains(lexeme) && Lexemes.Last().LexemeClass != LexemeClass.Type)
                    {
                        throw new Exception($"The variable '{lexeme}' doesn't exist");
                    }
                    else if(!Identifiers.Select(i => i.Name).Contains(lexeme) && Lexemes.Last().LexemeClass == LexemeClass.Type)
                    {
                        Identifiers.Add(new Identifier
                        {
                            Id = Identifiers.Count + 1,
                            Name = lexeme,
                            Type = Types.IndexOf(Lexemes.Last().Value)
                        });
                    }

                    Lexemes.Add(new Lexeme(lexemeClass, Identifiers.Select(i => i.Name).ToList().IndexOf(lexeme), locationInCode, lexeme));

                    return;                    
                case LexemeClass.Literal:
                    Literals.Add(lexeme);
                    break;
            }

            List<string> lexemesOfClass = (List<string>)typeof(LexemeStorage)
                                            .GetProperty($"{Enum.GetName(typeof(LexemeClass), lexemeClass)}s")
                                            .GetValue(this);

            var lexemeModel = new Lexeme(lexemeClass, lexemesOfClass.IndexOf(lexeme), locationInCode, lexeme);

            Lexemes.Add(lexemeModel);
        }

        public void Reset()
        {
            Identifiers.Clear();
            Literals.Clear();
            Lexemes.Clear();
        }
    }
}