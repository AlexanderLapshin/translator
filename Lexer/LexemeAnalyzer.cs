﻿using Lexer.Models;
using Newtonsoft.Json;
using Specs;
using Specs.Enums;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lexer
{
    public class LexemeAnalyzer
    {
        private FSM fsm;
        private List<Error> errors = new List<Error>();

        public  readonly LexemeStorage lexemeStorage;

        public LexemeAnalyzer()
        {
            lexemeStorage = JsonConvert.DeserializeObject<LexemeStorage>(File.ReadAllText("../../../Lexer/Resources/LexemeStorage.json"));
            fsm = new FSM();
        }

        /// <summary>
        /// Finds all lexemes and error in passed string using FSM
        /// </summary>
        /// <param name="code">
        /// A string of multiline code
        /// </param>
        /// <returns>
        /// Founded lexemes and errors in code
        /// </returns>
        public AnalyzeResult Analyze(string code)
        {
            Setup();
            int startPosition = 0;
            int endPosition = 0;

            int line = 1;
            int columnStart = 1;
            int columnEnd = 1;

            code += "\n";
            for (int pointer = 0; pointer < code.Length; pointer++)
            {
                char currentSymbol = code[pointer];
                int resultCode = fsm.HandleChar(currentSymbol);
                switch (resultCode)
                {
                    case FSM.LEXEME_HANDLING:
                        endPosition++;// go along lexeme
                        columnEnd++;
                        break;

                    case FSM.ERROR_STATE:
                        LocationInCode errorLocation = new LocationInCode(line, columnStart, columnEnd);
                        errors.Add(new Error("Lexical Error", errorLocation));
                        endPosition++;
                        startPosition = endPosition;
                        columnEnd++;
                        columnStart = columnEnd;
                        break;

                    case FSM.SKIP_WHITESPACE:
                        startPosition++;
                        endPosition++;
                        columnStart++;
                        columnEnd++;
                        break;

                    default:// class of lexeme
                        LocationInCode lexemeLocation = new LocationInCode(line, columnStart, columnEnd);
                        try
                        {
                            lexemeStorage.AddLexeme(code.Substring(startPosition, endPosition - startPosition), (LexemeClass)resultCode, lexemeLocation);
                        }
                        catch(Exception e)
                        {
                            errors.Add(new Error(e.Message, lexemeLocation));
                        }
                        pointer--;
                        startPosition = endPosition;
                        columnStart = columnEnd;
                        break;
                }

                if (code[pointer] == '\n')
                {
                    columnEnd = 1;
                    columnStart = 1;
                    line++;
                }
            }

            return new AnalyzeResult(lexemeStorage.Lexemes, lexemeStorage.Identifiers, errors);
        }

        private void Setup()
        {
            lexemeStorage.Reset();
            fsm.Reset();
        }
    }
}