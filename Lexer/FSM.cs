﻿using Lexer.Models;
using Newtonsoft.Json;
using Specs.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Lexer
{
    public class FSM
    {
        public const int ERROR_STATE = -500;
        public const int SKIP_WHITESPACE = -204;
        public const int LEXEME_HANDLING = -200;

        private int currentStateNumber = 0;
        private int resultStateNumber;
        private List<State> states;

        public FSM()
        {
            states = (List<State>)JsonConvert.DeserializeObject<IEnumerable<State>>(File.ReadAllText("../../../Lexer/Resources/StatesFSM.json"));
        }

        /// <summary>
        /// Takes a single char and finds the next state occording to current state
        /// </summary>
        /// <param name="symbol">
        /// A single symbol of code
        /// </param>
        /// <returns>
        /// A status or state code represented by int
        /// </returns>
        public int HandleChar(char symbol)
        {
            State currentState = states[currentStateNumber];

            for (int i = 0; i < currentState.NextStates.Count; i++)
            {
                if (currentState.NextStates[i].Value == null) // other symbols
                {
                    currentStateNumber = currentState.NextStates[i].StateNumber;
                    break;
                }
                else if (currentState.NextStates[i].Value.Length == 1)
                {
                    // change current State
                    if (currentState.NextStates[i].Value.Equals(symbol.ToString()))
                    {
                        currentStateNumber = currentState.NextStates[i].StateNumber;
                        break;
                    }
                }
                else if (currentState.NextStates[i].Value.Length > 1) // diapazon
                {
                    if (Regex.IsMatch(symbol.ToString(), "[" + currentState.NextStates[i].Value + "]"))
                    {
                        {
                            currentStateNumber = currentState.NextStates[i].StateNumber;
                            break;
                        }
                    }
                }
            }

            if (IsErrorOrLexemFound())
            {
                resultStateNumber = currentStateNumber;
                Reset();
            }
            else if (currentStateNumber == 0)
            {
                resultStateNumber = SKIP_WHITESPACE;
            }
            else resultStateNumber = LEXEME_HANDLING;

            return resultStateNumber;
        }

        public void Reset()
        {
            currentStateNumber = 0;
        }

        private bool IsErrorOrLexemFound()
        {
            if (currentStateNumber == ERROR_STATE)
                return true;

            foreach (var lexemeClass in Enum.GetValues(typeof(LexemeClass)))
            {
                if (currentStateNumber == (int)lexemeClass)
                    return true;
            }

            return false;
        }
    }
}