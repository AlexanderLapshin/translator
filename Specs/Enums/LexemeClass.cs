﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Specs.Enums
{
    public enum LexemeClass
    {
        ReservedWord = -1,
        Type = -2,
        Operator = -3,
        Separator = -4,
        Function = -5,
        Identifier = -6,
        Literal = -7,
        Special = -404

    }
}
