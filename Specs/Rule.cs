﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Specs
{
    public class Rule
    {
        public List<GrammarElement> GrammarElements { get; }

        public Rule(params GrammarElement[] grammarElements)
        {
            GrammarElements = new List<GrammarElement>();
            GrammarElements.AddRange(grammarElements);
        }

        public override string ToString()
        {
            string rule = "";
            foreach(GrammarElement element in GrammarElements)
            {
                rule += element.Value;
            }
            return rule;
        }
    }
}
