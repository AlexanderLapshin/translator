﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Specs
{
    public static class Helper
    {
        public static Lexeme Lambda { get; }
        public static Lexeme Stop { get; }

        static Helper()
        {
            Lambda = new Lexeme(Enums.LexemeClass.Special, 1, null, "e");
            Stop = new Lexeme(Enums.LexemeClass.Special, 2, null, "~");
        }

        public static bool LexemesEqual(Lexeme lex1, Lexeme lex2)
        {
            return lex1.LexemeClass == lex2.LexemeClass && lex1.NumberInClass == lex2.NumberInClass;
        }
    }
}
