﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Specs
{
    public abstract class GrammarElement
    {
        public string Value { get; set; }
        public HashSet<Lexeme> First { get; }

        public GrammarElement(string value)
        {
            Value = value;
            First = new HashSet<Lexeme>();
        }

        public abstract void BuildFirst();

        public override bool Equals(object obj)
        {
            return obj is GrammarElement grammarElement &&
                   Value == grammarElement.Value;
        }
    }
}
