﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Specs
{
    public delegate bool CheckFunction();
    public class Nonterminal : GrammarElement
    {
        public List<Rule> Rules { get; }
        public HashSet<Lexeme> Follow { get; }
        public CheckFunction CheckContextFunction { get; }

        public Nonterminal(string value, CheckFunction checkContextFunction = null) : base(value)
        {
            CheckContextFunction = checkContextFunction;
            Follow = new HashSet<Lexeme>();
            Rules = new List<Rule>();
        }

        public void AddRangeToHashSet(HashSet<Lexeme> elements, HashSet<Lexeme> range)
        {
            AddRangeToHashSet(elements, range.ToList());
        }
        

        public void AddRangeToHashSet(HashSet<Lexeme> elements, List<Lexeme> range)
        {
            foreach (Lexeme element in range)
            {
                elements.Add(element);
            }
        }

        public override void BuildFirst()
        {
            Lexeme lambda = Helper.Lambda;

            foreach(Rule rule in Rules)
            {
                bool nonstop = true;
                foreach(GrammarElement element in rule.GrammarElements)
                {
                    if(!nonstop)
                    {
                        break;
                    }
                    element.BuildFirst();
                    int count = First.Count;
                    AddRangeToHashSet(First, element.First);

                    if (element.First.Count() != 1 || !Helper.LexemesEqual(element.First.First(), lambda))
                    {
                        nonstop = false;
                    }
                }
                if(nonstop)
                {
                    First.Add(Helper.Lambda);
                }
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Nonterminal nonterminal && Value == nonterminal.Value;
        }
    }
}
