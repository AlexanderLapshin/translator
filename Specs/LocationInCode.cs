﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Specs
{
    public class LocationInCode : IComparable<LocationInCode>
    {
        public int Line { get; set; }
        public int ColumnStart { get; set; }
        public int ColumnEnd { get; set; }

        public LocationInCode(int line, int columnStart, int columnEnd)
        {
            Line = line;
            ColumnStart = columnStart;
            ColumnEnd = columnEnd;
        }

        public string Get()
        {
            return $"Line: {Line}. Location in line: [{ColumnStart}, {ColumnEnd}].";
        }

        public int CompareTo(LocationInCode other)
        {
            if (this.Line < other.Line)
                return -1;

            if (this.Line == other.Line)
            {
                if (this.ColumnStart < other.ColumnStart)
                    return -1;
                if (this.ColumnStart == other.ColumnStart)
                    return 0;
                return 1;
            }

            return 1;
        }
    }
}
