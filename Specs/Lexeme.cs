﻿using Specs.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Specs
{
    public class Lexeme : GrammarElement
    {
        public LexemeClass LexemeClass { get; }
        public int NumberInClass { get; }
        public LocationInCode LocationInCode { get; }

        public Lexeme(LexemeClass lexemeClass, int numberInClass, LocationInCode locationInCode, string value) : base(value)
        {
            LexemeClass = lexemeClass;
            NumberInClass = numberInClass;
            LocationInCode = locationInCode;
        }

        public Lexeme(string value, LexemeClass lexemeClass):this(lexemeClass, -404, null, value)
        {

        }

        public Lexeme(string value, LexemeClass lexemeClass, int numberInClass) : this(lexemeClass, numberInClass, null, value)
        {

        }

        public override void BuildFirst()
        {
            First.Add(this);
        }

        public override bool Equals(object obj)
        {
            return obj is Lexeme lexeme && Value == lexeme.Value;
        }
    }
}
